package com.bootdo.point.service;

import com.bootdo.point.domain.DeviceInfoDO;

import java.util.List;
import java.util.Map;

/**
 * 设备管理
 * 
 * @author langxianwei
 * @email 1992lcg@163.com
 * @date 2018-11-25 18:39:37
 */
public interface DeviceInfoService {
	
	DeviceInfoDO get(String deviceId);
	
	List<DeviceInfoDO> list(Map<String, Object> map);
	
	int count(Map<String, Object> map);
	
	int save(DeviceInfoDO deviceInfo);
	
	int update(DeviceInfoDO deviceInfo);
	
	int remove(String deviceId);
	
	int batchRemove(String[] deviceIds);
}
