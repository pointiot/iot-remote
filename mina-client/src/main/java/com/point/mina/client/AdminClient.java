package com.point.mina.client;

import java.nio.charset.CharacterCodingException;
import java.nio.charset.Charset;
import java.nio.charset.CharsetDecoder;
import java.nio.charset.CharsetEncoder;

import org.apache.mina.core.buffer.IoBuffer;

import com.point.iot.utils.Constant;

public class AdminClient{
	private ClientHanlder clientHanlder;
	private SocketClient mClient;
	public static AdminClient instance;

	private AdminClient() {
		// DBService连接
		clientHanlder = new ClientHanlder();
		mClient = new SocketClient("localhost", 65001, clientHanlder,0);
	}
	public static void main(String args[]){
		new AdminClient();
	}
	public static AdminClient getInstance() {
		if (instance != null) {
			return instance;
		}
		return instance = new AdminClient();
	}

	public void sendTemperatureData(int deviceId, long sessionId, short maxTemp, short minTemp) {
		IoBuffer buff = IoBuffer.allocate(100);
		buff.setAutoExpand(true);
		buff.put(Constant.ADMIN_TCP);
		buff.put((byte) 0x04);//设置温度
		buff.putInt(deviceId);
		buff.putLong(sessionId);
		buff.putShort(maxTemp);
		buff.putShort(minTemp);
		buff.flip();
		mClient.send(buff);
	}

}
